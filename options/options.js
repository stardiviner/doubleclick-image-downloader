"use strict";

const notify = document.getElementById("notify");
const oneClick = document.getElementById("oneClick");
const persist = document.getElementById("persist");
const requireShift = document.getElementById("requireShift");
const minimumImageSize = document.getElementById("minimumImageSize");
const excludedPageDomains = document.getElementById("excludedPageDomains");
const excludedSourceDomains = document.getElementById("excludedSourceDomains");
const singleClickEnabled = document.getElementById("singleClickEnabled");
const buttonSize = document.getElementById("buttonSize");
const buttonOpacity = document.getElementById("buttonOpacity");
const positionPicker = document.getElementById("positionPicker");
const enableRename = document.getElementById("enableRename");
const fileNamePattern = document.getElementById("fileNamePattern");
const counterPadding = document.getElementById("counterPadding");
const previewFileNamePattern = document.getElementById("previewFileNamePattern");
const enableSubfolder = document.getElementById("enableSubfolder");
const subfolderNamePattern = document.getElementById("subfolderNamePattern");
const previewSubfolderNamePattern = document.getElementById("previewSubfolderNamePattern");

excludedPageDomains.placeholder = excludedPageDomains.placeholder.replace(/\\n/g, "\n");
excludedSourceDomains.placeholder = excludedSourceDomains.placeholder.replace(/\\n/g, "\n");

const updatePreviewFileNamePattern = () => {
	const padding = parseInt(counterPadding.value);
	const number = "0".repeat(Math.max(0, padding - 1)) + "1";

	previewFileNamePattern.textContent = fileNamePattern.value
		.replace(/%original%/g, "some-image")
		.replace(/%pagedomain%/g, "blog.tumblr.com")
		.replace(/%imagedomain%/g, "cdn.imgur.com")
		.replace(/%title%/g, "My Album")
		.replace(/%counter%/g, number)
	;
};
const updatePreviwSubfolderNamePattern = () => {
	previewSubfolderNamePattern.textContent = subfolderNamePattern.value
		.replace(/%pagedomain%/g, "blog.tumblr.com")
		.replace(/%imagedomain%/g, "cdn.imgur.com")
		.replace(/%title%/g, "My Album")
	;
};

fileNamePattern.addEventListener("input", updatePreviewFileNamePattern);
counterPadding.addEventListener("change", updatePreviewFileNamePattern);
subfolderNamePattern.addEventListener("input", updatePreviwSubfolderNamePattern);

const updateSingleClickModeOptionsAttributes = () => {
	buttonSize.required = singleClickEnabled.checked;
	buttonSize.disabled = !buttonSize.required;

	buttonOpacity.required = buttonSize.required;
	buttonOpacity.disabled = buttonSize.disabled;

	Array.from(positionPicker.getElementsByTagName("input")).forEach(x => x.disabled = buttonSize.disabled);
};

const updateRenameOptionsAttributes = () => {
	fileNamePattern.required = enableRename.checked;
	fileNamePattern.disabled = !fileNamePattern.required;

	counterPadding.required = fileNamePattern.required;
	counterPadding.disabled = fileNamePattern.disabled;
};
const updateSubfolderOptionsAttributes = () => {
	subfolderNamePattern.required = enableSubfolder.checked;
	subfolderNamePattern.disabled = !enableSubfolder.checked;
};

updateSingleClickModeOptionsAttributes();
updateRenameOptionsAttributes();
updateSubfolderOptionsAttributes();

const domainNameRegex = /^(([\w\-]+|\*)\.)+([\w\-]+|\*)$/i;
const multiWhitespaceRegex = /\s+/g;

chrome.storage.sync.get({
	notify: true,
	oneClick: false,
	persist: false,
	requireShift: false,
	minimumImageSize: 100,
	excludedPageDomains: [],
	excludedSourceDomains: [],
	singleClickEnabled: false,
	buttonSize: 48,
	buttonOpacity: 1,
	buttonPosition: "2_3",
	enableRename: false,
	fileNamePattern: "%original%",
	counterPadding: 1,
	enableSubfolder: false,
	subfolderNamePattern: "%imagedomain%"
}, options => {
	notify.checked = options.notify;
	oneClick.checked = options.oneClick;
	persist.checked = options.persist;
	requireShift.checked = options.requireShift;
	minimumImageSize.value = options.minimumImageSize;
	excludedPageDomains.value = options.excludedPageDomains.join("\n");
	excludedSourceDomains.value = options.excludedSourceDomains.join("\n");
	singleClickEnabled.checked = options.singleClickEnabled;
	updateSingleClickModeOptionsAttributes();
	buttonSize.value = options.buttonSize;
	buttonOpacity.value = options.buttonOpacity * 100;
	document.querySelector("#positionPicker input[value='" + options.buttonPosition + "']").checked = true;
	enableRename.checked = options.enableRename;
	updateRenameOptionsAttributes();
	fileNamePattern.value = options.fileNamePattern;
	counterPadding.value = options.counterPadding;
	updatePreviewFileNamePattern();
	enableSubfolder.checked = options.enableSubfolder;
	updateSubfolderOptionsAttributes();
	subfolderNamePattern.value = options.subfolderNamePattern;
	updatePreviwSubfolderNamePattern();

	notify.addEventListener("change", event => {
		chrome.storage.sync.set({
			notify: notify.checked
		});
	});

	oneClick.addEventListener("change", event => {
		chrome.storage.sync.set({
			oneClick: oneClick.checked
		});
	});

	persist.addEventListener("change", event => {
		chrome.storage.sync.set({
			persist: persist.checked
		});
	});

	requireShift.addEventListener("change", event => {
		chrome.storage.sync.set({
			requireShift: requireShift.checked
		});
	});

	minimumImageSize.addEventListener("input", event => {
		if (minimumImageSize.checkValidity()) {
			chrome.storage.sync.set({
				minimumImageSize: parseInt(minimumImageSize.value)
			});
		}
	});

	excludedPageDomains.addEventListener("input", event => {
		chrome.storage.sync.set({
			excludedPageDomains: excludedPageDomains.value
				.split("\n")
				.filter(line => domainNameRegex.test(line))
		});
	});

	excludedPageDomains.addEventListener("blur", event => {
		chrome.storage.sync.get({
			excludedPageDomains: []
		}, data => {
			excludedPageDomains.value = data.excludedPageDomains.join("\n");
		});
	});

	excludedSourceDomains.addEventListener("input", event => {
		chrome.storage.sync.set({
			excludedSourceDomains: excludedSourceDomains.value
				.split("\n")
				.filter(line => domainNameRegex.test(line))
		});
	});

	excludedSourceDomains.addEventListener("blur", event => {
		chrome.storage.sync.get({
			excludedSourceDomains: []
		}, data => {
			excludedSourceDomains.value = data.excludedSourceDomains.join("\n");
		});
	});

	singleClickEnabled.addEventListener("change", event => {
		updateSingleClickModeOptionsAttributes();
		chrome.storage.sync.set({
			singleClickEnabled: singleClickEnabled.checked
		});
	});

	buttonSize.addEventListener("input", event => {
		if (buttonSize.checkValidity()) {
			chrome.storage.sync.set({
				buttonSize: parseInt(buttonSize.value)
			});
		}
	});

	buttonOpacity.addEventListener("input", event => {
		if (buttonOpacity.checkValidity()) {
			chrome.storage.sync.set({
				buttonOpacity: parseInt(buttonOpacity.value)/100
			});
		}
	});

	positionPicker.addEventListener("change", event => {
		chrome.storage.sync.set({
			buttonPosition: event.target.value
		});
	});

	enableRename.addEventListener("change", event => {
		updateRenameOptionsAttributes();
		const option = {
			enableRename: enableRename.checked
		};
		if (enableRename.checked && fileNamePattern.value.trim().replace(multiWhitespaceRegex, " ").length === 0) {
			option.fileNamePattern = "%original%";
			fileNamePattern.value = option.fileNamePattern;
			updatePreviewFileNamePattern();
		}
		chrome.storage.sync.set(option);
	});

	fileNamePattern.addEventListener("input", event => {
		if (fileNamePattern.checkValidity()) {
			chrome.storage.sync.set({
				fileNamePattern: fileNamePattern.value.trim().replace(multiWhitespaceRegex, " ")
			});
		}
	});

	counterPadding.addEventListener("change", event => {
		if (counterPadding.checkValidity()) {
			chrome.storage.sync.set({
				counterPadding: parseInt(counterPadding.value)
			});
		}
	});

	enableSubfolder.addEventListener("change", event => {
		updateSubfolderOptionsAttributes();
		const option = {
			enableSubfolder: enableSubfolder.checked
		};
		if (enableSubfolder.checked && subfolderNamePattern.value.trim().replace(multiWhitespaceRegex, " ").length === 0) {
			option.subfolderNamePattern = "%imagedomain%";
			subfolderNamePattern.value = option.subfolderNamePattern;
			updatePreviwSubfolderNamePattern();
		}
		chrome.storage.sync.set(option);
	});

	subfolderNamePattern.addEventListener("input", event => {
		if (subfolderNamePattern.checkValidity()) {
			chrome.storage.sync.set({
				subfolderNamePattern: subfolderNamePattern.value.trim().replace(multiWhitespaceRegex, " ")
			});
		}
	});
});
