"use strict";

const Tree = (() => {
	const parseStrings = strings => {
		const filters = strings.map(filter => filter.split(".").reverse()).map(filter => {
			return {
				tld: filter[0],
				tail: filter.slice(1)
			};
		});

		const out = {};

		for (const filter of filters) {
			const tldChildren = out[filter.tld] || (() => {
					const list = [];
					out[filter.tld] = list;
					return list;
				})();
			tldChildren.push(filter.tail);
		}

		return out;
	};

	const contains = (tree, subject) => {
		const target = (() => {
			const split = subject.split(".").reverse();

			return {
				tld: split[0],
				tail: split.slice(1)
			};
		})();

		const tldChildren = tree[target.tld];
		if (tldChildren !== undefined) {
			for (const filter of tldChildren) {
				if (filter.length === target.tail.length) {
					for (let i = 0; i < target.tail.length; i++) {
						if (target.tail[i] === filter[i] || filter[i] === "*") {
							if (i === target.tail.length - 1) {
								return true;
							}
						} else {
							break;
						}
					}
				}
			}
			return false;
		} else {
			return false;
		}
	};

	return function(strings) {
		this.tree = parseStrings(strings);
		this.contains = f => contains(this.tree, f);
	};
})();

let cachedPreferences = {
	oneClick: false,
	persist: false,
	buttonSize: 48,
	buttonOpacity: 1,
	requireShift: false,
	singleClickEnabled: false,
	minimumImageSize: 100,
	excludedPageDomains: [],
	excludedSourceDomains: [],
	buttonPosition: "2_3",
	debugging: false
};

const parseDomainTrees = () => {
	cachedPreferences.excludedPageDomainsParsed = new Tree(cachedPreferences.excludedPageDomains);
	cachedPreferences.excludedSourceDomainsParsed = new Tree(cachedPreferences.excludedSourceDomains);
};
parseDomainTrees();

const sendURL = (() => {
	const activeClass = "doubleclick-image-downloader-active";

	return img => {
		let id = null;

		const listener = (msg, sender) => {
			if (!sender.tab && msg.subject === "downloadFinished" && msg.id === id) {
				chrome.runtime.onMessage.removeListener(listener);

				img.classList.remove(activeClass);
			}
		};

		chrome.runtime.onMessage.addListener(listener);

		chrome.runtime.sendMessage({
			subject: "downloadRequested",
			url: img.src
		}, response => {
			img.classList.add(activeClass);
			id = response.id;
		});
	};
})();

const dlButton = (() => {
	const htmlId = "singleclick-image-downloader";
	let currentImg = null;

	const buttonOnUrl = chrome.extension.getURL("/images/download_on.png");
	const buttonOnProperty = "url('" + buttonOnUrl + "')";
	const buttonOffUrl = chrome.extension.getURL("/images/download_off.png");
	const buttonOffProperty = "url('" + buttonOffUrl + "')";

	const style = document.createElement("style");
	document.head.appendChild(style);
	const sheet = style.sheet;
	sheet.insertRule("#" + htmlId + "{background-image: " + buttonOffProperty + ";}", 0);
	sheet.insertRule("#" + htmlId + ":hover {background-image: " + buttonOnProperty + ";}", 1);

	let isInserted = false;

	const button = document.createElement("div");
	button.id = htmlId;

	const setOpacity = opacity => {
		button.style.opacity = opacity;
	};
	setOpacity(1);

	const setSize = size => {
		button.style.height = size + "px";
		button.style.width = size + "px";
	};
	setSize(48);

	button.addEventListener("click", event => {
		event.stopPropagation();
		sendURL(currentImg);
	}, true);

	const determineX = (position, rect) => {
		switch (position) {
			case "1_1":
			case "1_3":
			case "3":
			case "3_1":
			case "3_3":
				return rect.left - cachedPreferences.buttonSize;

			case "1_2":
			case "1_4":
			case "4":
			case "3_2":
			case "3_4":
				return rect.left;

			case "1":
			case "2":
			case "5":
			case "8":
			case "9":
				return rect.left + (rect.width - cachedPreferences.buttonSize)/2;

			case "2_1":
			case "2_3":
			case "6":
			case "4_1":
			case "4_3":
				return rect.right - cachedPreferences.buttonSize;

			case "2_2":
			case "2_4":
			case "7":
			case "4_2":
			case "4_4":
				return rect.right;
		}
	};

	const determineY = (position, rect) => {
		switch (position) {
			case "1_1":
			case "1_2":
			case "1":
			case "2_1":
			case "2_2":
				return rect.top - cachedPreferences.buttonSize;

			case "1_3":
			case "1_4":
			case "2":
			case "2_3":
			case "2_4":
				return rect.top;

			case "3":
			case "4":
			case "5":
			case "6":
			case "7":
				return rect.top + (rect.height - cachedPreferences.buttonSize)/2;

			case "3_1":
			case "3_2":
			case "8":
			case "4_1":
			case "4_2":
				return rect.bottom - cachedPreferences.buttonSize;

			case "3_3":
			case "3_4":
			case "9":
			case "4_3":
			case "4_4":
				return rect.bottom;
		}
	};

	const offset = position => {
		const rect = currentImg.getBoundingClientRect();

		const coordinates = {
			x: determineX(position, rect),
			y: determineY(position, rect)
		};

		button.style.left = Math.max(0, Math.min(document.documentElement.clientWidth - cachedPreferences.buttonSize, coordinates.x)) + "px";
		button.style.top = Math.max(0, Math.min(document.documentElement.clientHeight - cachedPreferences.buttonSize, coordinates.y)) + "px";
	};

	const setVisible = (visible, position) => {
		if (visible) {
			offset(position); // may need to update position to neighboring image
		}
		if (isInserted !== visible) {
			if (visible) {
				document.body.appendChild(button);
				document.addEventListener("scroll", event => offset(position));
			} else {
				document.body.removeChild(button);
				document.removeEventListener("scroll", event => offset(position));
			}
			isInserted = visible;
		}
	};

	const onMouseOver = event => {
		if (event.target !== button && event.target !== currentImg) {
			if (
				event.target.nodeName === "IMG"
				&& !cachedPreferences.excludedPageDomainsParsed.contains(location.hostname)
				&& (!cachedPreferences.requireShift || cachedPreferences.requireShift && event.shiftKey)
				&& event.target.width > cachedPreferences.minimumImageSize && event.target.height > cachedPreferences.minimumImageSize
			) {
				const url = new URL(event.target.src);

				if (!cachedPreferences.excludedSourceDomainsParsed.contains(url.hostname)) {
					currentImg = event.target;
					setVisible(true, cachedPreferences.buttonPosition);
				}
			} else {
				if (!cachedPreferences.persist || cachedPreferences.persist && event.target.nodeName === "IMG") {
					currentImg = null;
					setVisible(false);
				}
			}
		}
	};

	const setEnabled = state => {
		if (state) {
			document.addEventListener("mouseover", onMouseOver);
		} else {
			document.removeEventListener("mouseover", onMouseOver);
			currentImg = null;
			setVisible(false);
		}
	};

	return {
		setSize: setSize,
		setOpacity: setOpacity,
		setEnabled: setEnabled
	};
})();

const listener = (() => {
	const recurseForImage = element => {
		for (let i = 0; i < element.childNodes.length; i++) {
			const child = element.childNodes[i];
			if (child.nodeName === "IMG") {
				return child;
			} else {
				const result = recurseForImage(child);
				if (result) {
					return result;
				}
			}
		}
		return false;
	};

	const onClick = event => {
		if (
			event.target.nodeName === "IMG"
			&& (!cachedPreferences.requireShift || cachedPreferences.requireShift && event.shiftKey)
			&& !cachedPreferences.excludedPageDomainsParsed.contains(location.hostname)
		) {
			if (event.target.width > cachedPreferences.minimumImageSize && event.target.height > cachedPreferences.minimumImageSize) {
				const url = new URL(event.target.src);
				if (!~cachedPreferences.excludedSourceDomains.indexOf(url.hostname)) {
					event.stopPropagation();
					sendURL(event.target);
				}
			} else {
				const img = recurseForImage(event.target);
				if (img && img.width > cachedPreferences.minimumImageSize && img.height > cachedPreferences.minimumImageSize) {
					const url = new URL(img.src);
					if (!cachedPreferences.excludedSourceDomainsParsed.contains(url.hostname)) {
						event.stopPropagation();
						sendURL(img);
					}
				}
			}
		}
	};

	const apply = () => {
		document.removeEventListener("click", onClick);
		document.removeEventListener("dblclick", onClick);
		document.addEventListener(cachedPreferences.oneClick? "click": "dblclick", onClick, true);
	};

	return {
		apply: apply
	};
})();

chrome.storage.sync.get(cachedPreferences, options => {
	cachedPreferences = options;
	parseDomainTrees();

	dlButton.setSize(options.buttonSize);
	dlButton.setOpacity(options.buttonOpacity);
	dlButton.setEnabled(options.singleClickEnabled);

	listener.apply();

	chrome.storage.onChanged.addListener((changes, areaName) => {
		if (areaName === "sync") {
			Object.keys(changes).forEach(property => {
				if (cachedPreferences[property] !== undefined) {
					const newValue = changes[property].newValue;
					cachedPreferences[property] = newValue;

					switch (property) {
						case "buttonOpacity":
							dlButton.setOpacity(newValue);
							break;

						case "buttonSize":
							dlButton.setSize(newValue);
							break;

						case "singleClickEnabled":
							dlButton.setEnabled(newValue);
							break;

						case "excludedPageDomains":
						case "excludedSourceDomains":
							parseDomainTrees();
							break;

						case "oneClick":
							listener.apply();
					}
				}
			});
		}

		if (areaName === "local") {
			Object.keys(changes).forEach(property => {
				if (cachedPreferences[property] !== undefined) {
					cachedPreferences[property] = changes[property].newValue;
				}
			});
		}
	});
});
